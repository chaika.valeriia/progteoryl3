import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm;

  constructor(
    private formBuilder: FormBuilder, private router: Router) 
    {
      this.loginForm = this.formBuilder.group({
        username: "",
        password: ""
      });
    }
  
  ngOnInit() {}

  onSubmit(loginData) {
    // Process login here
    this.loginForm.reset();
    if(loginData.username == "Admin" && loginData.password == "12345"){
      this.router.navigate(['profile']);
      localStorage["logined"] = true;
    }
    else {
      alert("Username or password is incorrect");
      localStorage["logined"] = false;
    }
  }
}
